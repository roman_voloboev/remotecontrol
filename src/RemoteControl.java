import javax.bluetooth.*;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;
import javax.microedition.lcdui.*;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;


public class RemoteControl extends MIDlet implements CommandListener, DiscoveryListener {
    private Command cmdExit;
    private Command cmdSearch;
    private Command cmdBack;
    private Form mainForm;
    private Form connectionForm;
    private Display display;
    private StringItem message;
    private StringItem info;
    private StringItem status;
    private List devicesList;
    private Vector remoteDevices;
    private StreamConnection connection;
    private OutputStream out;
    private InputStream in;
    private boolean notFound;

    RemoteControl() {
        devicesList = new List("Выберите устройство",Choice.IMPLICIT);
        mainForm = new Form("Arduino Bluetooth");
        connectionForm = new Form("Connection");

        message = new StringItem("", "");
        message.setLayout(StringItem.LAYOUT_CENTER);
        mainForm.append(message);

        info = new StringItem("Подключен: ", "");
        info.setLayout(StringItem.LAYOUT_LEFT);
        connectionForm.append(info);

        cmdExit = new Command("Выход", Command.EXIT, 1);
        cmdSearch = new Command("Поиск", Command.BACK, 1);
        cmdBack = new Command("Назад", Command.BACK, 1);

        mainForm.addCommand(cmdExit);
        connectionForm.addCommand(cmdBack);
        devicesList.addCommand(cmdSearch);

        searchDevices();
    }

    protected void startApp() throws MIDletStateChangeException {
        if (display == null) {
            display = Display.getDisplay(this);
        }
        mainForm.setCommandListener(this);
        connectionForm.setCommandListener(this);
        devicesList.setCommandListener(this);
        display.setCurrent(mainForm);
    }

    public void searchDevices() {
        message.setText("Поиск устройств...");
        remoteDevices = new Vector();
        devicesList.deleteAll();
        try {
            LocalDevice localDevice = LocalDevice.getLocalDevice();
            DiscoveryAgent discoveryAgent = localDevice.getDiscoveryAgent();
            discoveryAgent.startInquiry(DiscoveryAgent.GIAC, this);
        } catch (BluetoothStateException e) {
            e.printStackTrace();
        }
    }

    public void deviceDiscovered(RemoteDevice remoteDevice, DeviceClass deviceClass) {
        try {
            devicesList.append((remoteDevice.getFriendlyName(false)), null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        remoteDevices.addElement(remoteDevice);
    }

    public void inquiryCompleted(int param) {
        switch (param) {
            case DiscoveryListener.INQUIRY_COMPLETED:
                if (remoteDevices.size() > 0) {
                    display.setCurrent(devicesList);
                } else {
                     message.setText("Устройства не найдены");
                     notFound = true;
                     mainForm.addCommand(cmdSearch);
                }
                break;
            case DiscoveryListener.INQUIRY_ERROR:
                message.setText("Bluetooth отключен");
                break;
            case DiscoveryListener.INQUIRY_TERMINATED:
                message.setText("Поиск отменен");
                break;
        }
    }

    public void connect(int selectedIndex) throws IOException {
        if (connection == null) {
            RemoteDevice remoteDevice = (RemoteDevice)remoteDevices.elementAt(selectedIndex);
            String btConnectionURL = "btspp://" + remoteDevice.getBluetoothAddress() + ":1;authenticate=false;encrypt=false;master=false";
            try {
                connection = (StreamConnection) Connector.open(btConnectionURL);
                out = connection.openOutputStream();
                in = connection.openDataInputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
            info.setText(remoteDevice.getFriendlyName(false)+" ("+remoteDevice.getBluetoothAddress()+")");
        }
    }

//    public void send() {
//        connect();
//
//    }

    public void disconnect() {
        try {
            out.close();
            in.close();
            connection.close();
            connection = null;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void commandAction(Command command, Displayable displayable) {
        if (command == cmdExit) {
            try {
                destroyApp(true);
            } catch (MIDletStateChangeException e) {
                e.printStackTrace();
            }
        } else if (command == cmdSearch) {
            if (notFound) {
                mainForm.removeCommand(cmdSearch);
                notFound = false;
            }
            searchDevices();
            display.setCurrent(mainForm);
        } else if (command == cmdBack) {
            disconnect();
            display.setCurrent(devicesList);
        }
        if (command == List.SELECT_COMMAND) {
            //alert("select pressed",1000);
            try {
                connect(devicesList.getSelectedIndex());
            } catch (IOException e) {
                e.printStackTrace();
            }
            display.setCurrent(connectionForm);
        }

    }

    public void alert(String msg,int time_out){
        if (display.getCurrent() instanceof Alert) {
            ((Alert)display.getCurrent()).setString(msg);
            ((Alert)display.getCurrent()).setTimeout(time_out);
        } else {
            Alert alert = new Alert("Bluetooth");
            alert.setString(msg);
            alert.setTimeout(time_out);
            display.setCurrent(alert);
        }
    }

    public void serviceSearchCompleted(int i, int i2) {
    }

    public void servicesDiscovered(int i, ServiceRecord[] serviceRecords) {
    }

    protected void destroyApp(boolean b) throws MIDletStateChangeException {
        notifyDestroyed();
    }

    protected void pauseApp() {
        notifyPaused();
    }
}
